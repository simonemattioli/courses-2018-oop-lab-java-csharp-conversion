using System;
using System.Collections;
using System.Collections.Generic;

namespace Unibo.Oop.Utils
{
    public class ImmutableMatrixDecorator<TElem> : IImmutableMatrix<TElem>
    {
        private readonly IBaseMatrix<TElem> decorated;

        public ImmutableMatrixDecorator(IBaseMatrix<TElem> decorated) 
        {
            this.decorated = decorated ?? throw new InvalidOperationException();
        }

        public TElem this[int i, int j] => throw new NotImplementedException();

        public int ColumnsCount => throw new NotImplementedException();

        public int RowsCount => throw new NotImplementedException();

        public int Count => throw new NotImplementedException();

        public int CoordAntidiagonal(int i, int j)
        {
            throw new NotImplementedException();
        }

        public int CoordColumn(int d, int a)
        {
            throw new NotImplementedException();
        }

        public int CoordDiagonal(int i, int j)
        {
            throw new NotImplementedException();
        }

        public int CoordRow(int d, int a)
        {
            throw new NotImplementedException();
        }

        public void ForEachIndexed(Action<int, int, TElem> consumer)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TElem> GetAntidiagonal(int a)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TElem> GetColumn(int j)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TElem> GetDiagonal(int d)
        {
            throw new NotImplementedException();
        }

        public TElem GetDiagonals(int d, int a)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<TElem> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TElem> GetRow(int i)
        {
            throw new NotImplementedException();
        }

        public TElem[] ToArray()
        {
            throw new NotImplementedException();
        }

        public IList<TElem> ToList()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}