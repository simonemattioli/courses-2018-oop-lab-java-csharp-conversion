package it.unibo.oop.mnk;

import it.unibo.oop.events.EventEmitter;
import it.unibo.oop.events.EventSource;
import it.unibo.oop.util.ImmutableMatrix;
import it.unibo.oop.util.Matrix;
import it.unibo.oop.util.SequenceUtils;
import it.unibo.oop.util.Tuple3;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

class MNKMatchImpl implements MNKMatch {
    private final Matrix<Symbols> grid;
    private final int k;
    private final EventEmitter<TurnEventArgs> turnEnded = EventEmitter.unordered();
    private final EventEmitter<TurnEventArgs> turnBeginning = EventEmitter.unordered();
    private final EventEmitter<MatchEventArgs> matchEnded = EventEmitter.unordered();
    private final EventEmitter<Exception> errorOccurred = EventEmitter.unordered();
    private final EventEmitter<MNKMatch> resetPerformed = EventEmitter.unordered();
    private int turn;
    private boolean ended = false;
    private Symbols player;

    MNKMatchImpl(int m, int n, int k) {
        this(m, n, k, Symbols.pickRandomly());
    }

    MNKMatchImpl(int m, int n, int k, Symbols first) {
        if (k > Math.min(m, n)) {
            throw new IllegalArgumentException("Required: k <= min(m, n)");
        }
        this.grid = Matrix.of(m, n, Symbols.EMPTY);
        this.k = k;
        this.player = Objects.requireNonNull(first);
        this.turn = 1;
    }

    private static Symbols nextPlayer(Symbols player) {
        if (player == Symbols.EMPTY)
            throw new IllegalArgumentException();
        else if (player == Symbols.CROSS)
            return Symbols.ROUND;
        else
            return Symbols.CROSS;
    }

    @Override
    public ImmutableMatrix<Symbols> getGrid() {
        return ImmutableMatrix.wrap(grid);
    }

    @Override
    public int getK() {
        return k;
    }

    protected Optional<Symbols> checkVictory(int i, int j, Symbols currentPlayer) {
        final int d = grid.coordDiagonal(i, j);
        final int a = grid.coordAntidiagonal(i, j);

        final List<Stream<Symbols>> lists = Arrays.asList(
            grid.getRow(i),
            grid.getColumn(j),
            grid.getDiagonal(d),
            grid.getAntidiagonal(a)
        );

        return lists.stream()
                .map(this::kAligned)
                .filter(Optional::isPresent)
                .map(x -> x.map(Tuple3::getThird))
                .findAny()
                .orElse(Optional.empty());
    }

    protected Optional<Tuple3<Integer, Integer, Symbols>> kAligned(Stream<Symbols> data) {
        final List<Tuple3<Integer, Integer, Symbols>> lst = SequenceUtils.subsequences(data);
        return lst.stream()
                .filter(t -> t.getThird() != Symbols.EMPTY)
                .filter(t -> t.getSecond() >= getK())
                .findAny();

    }

    private void beforeMoving(int turn, int i, int j, Symbols currentPlayer) {
        if (grid.get(i, j) != Symbols.EMPTY)
            throw new IllegalStateException(String.format("Cell (%d, %d) is not empty!", i, j));
    }

    private void afterMoved(int turn, int i, int j, Symbols currentPlayer) {
        final Optional<Symbols> winner = checkVictory(i, j, currentPlayer);
        if (winner.isPresent() || turn >= grid.size()) {
            ended = true;
            onMatchEnded(turn, i, j, winner);
        } else {
            onTurnEnded(turn, i, j, currentPlayer);
        }
    }

    protected void onMatchEnded(int currentTurn, int i, int j, Optional<Symbols> winner) {
        matchEnded.emit(new MatchEventArgs(this, currentTurn, winner.orElse(null), i, j));
    }

    protected void onTurnEnded(int currentTurn, int i, int j, Symbols currentPlayer) {
        turnEnded.emit(new TurnEventArgs(this, currentTurn, currentPlayer, i, j));
        turn++;
        player = nextPlayer(player);
        turnBeginning.emit(new TurnEventArgs(this, getTurn(), getCurrentPlayer(), i, j));
    }

    @Override
    public void move(int i, int j) {
        try {
            if (ended) {
                throw new IllegalStateException("Match is over!");
            }
            beforeMoving(turn, i, j, player);
            grid.set(i, j, player);
            afterMoved(turn, i, j, player);
        } catch (Exception ex) {
            errorOccurred.emit(ex);
        }

    }

    @Override
    public void reset() {
        grid.setAll(Symbols.EMPTY);
        player = turn % 2 == 0 ? nextPlayer(player) : player;
        turn = 1;
        ended = false;
        resetPerformed.emit(this);
    }

    @Override
    public EventSource<TurnEventArgs> turnEnded() {
        return turnEnded.getEventSource();
    }

    @Override
    public EventSource<MatchEventArgs> matchEnded() {
        return matchEnded.getEventSource();
    }

    @Override
    public int getTurn() {
        return turn;
    }

    @Override
    public Symbols getCurrentPlayer() {
        return player;
    }

    @Override
    public EventSource<TurnEventArgs> turnBeginning() {
        return turnBeginning.getEventSource();
    }

    @Override
    public EventSource<Exception> errorOccurred() {
        return errorOccurred.getEventSource();
    }

    @Override
    public EventSource<MNKMatch> resetPerformed() {
        return resetPerformed.getEventSource();
    }
}
